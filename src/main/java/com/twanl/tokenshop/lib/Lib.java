package com.twanl.tokenshop.lib;

import com.twanl.tokens.api.TokensAPI;
import com.twanl.tokenshop.util.ConfigManager;
import com.twanl.tokenshop.util.Strings;

public class Lib {
    private TokensAPI A = new TokensAPI();

    public void setDefaultSettings() {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();





        if (!cfgM.getData().isSet("menu")) {
            cfgM.getData().set("menu.options.slots", 36);
            cfgM.getData().set("menu.options.name", "&2&lToken Shop");

            cfgM.getData().set("menu.slots.1.slot", 11);
            cfgM.getData().set("menu.slots.1.name", "&6Blocks");
            cfgM.getData().set("menu.slots.1.amount", 1);
            cfgM.getData().set("menu.slots.1.item", 2);
            cfgM.getData().set("menu.slots.1.data", 0);
            cfgM.getData().set("menu.slots.1.command", "<*> blocks");
            cfgM.getData().set("menu.slots.1.permission", "example.permission.blocks");

            cfgM.getData().set("menu.slots.2.slot", 31);
            cfgM.getData().set("menu.slots.2.name", "&aClose");
            cfgM.getData().set("menu.slots.2.amount", 1);
            cfgM.getData().set("menu.slots.2.item", 160);
            cfgM.getData().set("menu.slots.2.data", 14);
            cfgM.getData().set("menu.slots.2.command", "<>");



            cfgM.getData().set("blocks.options.slots", 54);
            cfgM.getData().set("blocks.options.name", "&aBlocks");

            cfgM.getData().set("blocks.slots.1.slot", 0);
            cfgM.getData().set("blocks.slots.1.name", "&6Cobblestone");
            cfgM.getData().set("blocks.slots.1.amount", 1);
            cfgM.getData().set("blocks.slots.1.item", 4);
            cfgM.getData().set("blocks.slots.1.data", 0);
            cfgM.getData().set("blocks.slots.1.price", 10);
            cfgM.getData().set("blocks.slots.1.command", "give {player} 4 1");

            cfgM.getData().set("blocks.slots.2.slot", 53);
            cfgM.getData().set("blocks.slots.2.name", "&aBack");
            cfgM.getData().set("blocks.slots.2.amount", 1);
            cfgM.getData().set("blocks.slots.2.item", 160);
            cfgM.getData().set("blocks.slots.2.data", 14);
            cfgM.getData().set("blocks.slots.2.command", "<*> menu");



            cfgM.saveData();
        }


    }



    public String getTitle(String menu) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return (String) cfgM.getData().get(menu + ".options.name");
    }

    public String getCommand(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return (String) cfgM.getData().get(menu + ".slots." + i + ".command");
    }

    public int getSlotAmount(String menu) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return cfgM.getData().getInt(menu + ".options.slots");
    }

    public int getAmount(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return cfgM.getData().getInt(menu + ".slots." + i + ".amount");
    }

    public int getSlot(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return cfgM.getData().getInt(menu + ".slots." + i + ".slot");
    }

    public int getItem(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return cfgM.getData().getInt(menu + ".slots." + i + ".item");
    }

    public int getByte(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return cfgM.getData().getInt(menu + ".slots." + i + ".data");
    }

    public Short getByte1(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        short a = (short) cfgM.getData().getInt(menu + ".slots." + i + ".data");
        return a;
    }

    public int getPrice(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return cfgM.getData().getInt(menu + ".slots." + i + ".price");
    }

    public String getItemName(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return (String) cfgM.getData().get(menu + ".slots." + i + ".name");
    }

    public String priceStatus1(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        if (!cfgM.getData().isSet(menu + ".slots." + i + ".price")) {
            return "";
        } else {
            return " ";
        }
    }

    public String priceStatus(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        if (!cfgM.getData().isSet(menu + ".slots." + i + ".price")) {
            return "";
        } else {
            return Strings.gray +"Price: " + Strings.green + getPrice(menu, i) + " " + A.getPrefix();
        }
    }

    public String getPermission(String menu, int i) {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();


        return (String) cfgM.getData().get(menu + ".slots." + i + ".permission");
    }



}
