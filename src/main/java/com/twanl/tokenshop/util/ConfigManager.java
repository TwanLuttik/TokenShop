package com.twanl.tokenshop.util;

import com.twanl.tokenshop.TokenShop;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {


    private TokenShop plugin = TokenShop.getPlugin(TokenShop.class);

    //Files & Config Files
    private static FileConfiguration locationC;
    private static File locationF;
    //------------------


    public void setup() {
        locationF = new File(plugin.getDataFolder(), "data.yml");
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }


        if (!locationF.exists()) {
            try {
                locationF.createNewFile();
                Bukkit.getServer().getConsoleSender().sendMessage(Strings.green + "The data.yml file has been created");
            } catch (IOException e) {
                Bukkit.getServer().getConsoleSender().sendMessage(Strings.red + "Could not create the data.yml file");
            }
        }

        locationC = YamlConfiguration.loadConfiguration(locationF);

    }


    public FileConfiguration getData() {
        return locationC;

    }



    public void saveData() {
        locationF = new File(plugin.getDataFolder(), "data.yml");

        try {
            locationC.save(locationF);
            Bukkit.getServer().getConsoleSender().sendMessage(Strings.green + "The data.yml file has been saved");

        } catch (IOException e) {
            Bukkit.getServer().getConsoleSender().sendMessage(Strings.red + "Could not save the data.yml file");

        }
    }


    public void reloadData() {
        locationC = YamlConfiguration.loadConfiguration(locationF);
        Bukkit.getServer().getConsoleSender().sendMessage(Strings.green + "The data.yml file has been reloaded");

    }


}
