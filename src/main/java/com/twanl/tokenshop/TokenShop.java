package com.twanl.tokenshop;


import com.twanl.tokenshop.NMS.VersionHandler;
import com.twanl.tokenshop.NMS.v1_10.v1_10_R1;
import com.twanl.tokenshop.NMS.v1_11.v1_11_R1;
import com.twanl.tokenshop.NMS.v1_12.v1_12_R1;
import com.twanl.tokenshop.NMS.v1_13.v1_13_R1;
import com.twanl.tokenshop.NMS.v1_8.v1_8_R1;
import com.twanl.tokenshop.NMS.v1_8.v1_8_R2;
import com.twanl.tokenshop.NMS.v1_8.v1_8_R3;
import com.twanl.tokenshop.NMS.v1_9.v1_9_R1;
import com.twanl.tokenshop.NMS.v1_9.v1_9_R2;
import com.twanl.tokenshop.events.JoinEvent;
import com.twanl.tokenshop.lib.Lib;
import com.twanl.tokenshop.menu.mainMenu;
import com.twanl.tokenshop.util.ConfigManager;
import com.twanl.tokenshop.util.Strings;
import com.twanl.tokenshop.util.UpdateChecker;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class TokenShop extends JavaPlugin {


    protected PluginDescriptionFile pdfFile = getDescription();
    private final String PluginVersionOn = Strings.green + "(" + pdfFile.getVersion() + ")";
    private final String PluginVersionOff = Strings.red + "(" + pdfFile.getVersion() + ")";

    public VersionHandler nms;
    private UpdateChecker checker;
    private Lib lib = new Lib();
    public mainMenu menuApi;



    public void onEnable() {

        getServerVersion();


        if (getServer().getPluginManager().getPlugin("Tokens") != null) {
        } else {
            getServer().getConsoleSender().sendMessage(Strings.green + Strings.logName + Strings.red + "TokenShop not found, this plugin will be disabled!");
            Bukkit.getServer().getPluginManager().disablePlugins();
        }

        checker = new UpdateChecker(this);
        if (checker.isConnected()) {
            if (checker.hasUpdate()) {
                getServer().getConsoleSender().sendMessage(Strings.green + "");
                getServer().getConsoleSender().sendMessage(Strings.green + "------------------------");
                getServer().getConsoleSender().sendMessage(Strings.red + "TokenShop is outdated!");
                getServer().getConsoleSender().sendMessage(Strings.white + "Newest version: " + checker.getLatestVersion());
                getServer().getConsoleSender().sendMessage(Strings.white + "Your version: " + Strings.green + getDescription().getVersion());
                getServer().getConsoleSender().sendMessage("Please download the new version at https://www.spigotmc.org/resources/tokens-shop.57866/");
                getServer().getConsoleSender().sendMessage(Strings.green + "------------------------");
                getServer().getConsoleSender().sendMessage(Strings.green + "");
            } else {
                getServer().getConsoleSender().sendMessage(Strings.green + "");
                getServer().getConsoleSender().sendMessage(Strings.green + "---------------------------------");
                getServer().getConsoleSender().sendMessage(Strings.green + "TokenShop is up to date.");
                getServer().getConsoleSender().sendMessage(Strings.green + "---------------------------------");
                getServer().getConsoleSender().sendMessage(Strings.green + "");
            }
        }


        menuApi = new mainMenu();

        lib.setDefaultSettings();


        Load();
        LoadConfig();
        Bukkit.getServer().getConsoleSender().sendMessage(Strings.green + Strings.logName + Strings.green + "Has been enabled " + PluginVersionOn);
    }

    public void onDisable() {


        Bukkit.getServer().getConsoleSender().sendMessage(Strings.green + Strings.logName + Strings.red + "Has been disabled " + PluginVersionOff);
    }


    private void LoadConfig() {
        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();
        cfgM.saveData();
        cfgM.reloadData();
    }


    public void Load(){
        // Register listeners
        getServer().getPluginManager().registerEvents(new mainMenu(), this);
            getServer().getPluginManager().registerEvents(new JoinEvent(), this);


        //LoadConfig
        getConfig().options().copyDefaults(true);
        saveConfig();

    }


    private void getServerVersion() {
        String a = getServer().getClass().getPackage().getName();
        String version = a.substring(a.lastIndexOf('.') + 1);

        // Check
        if (version.equalsIgnoreCase("v1_8_R1")) {
            nms = new v1_8_R1();
        } else if (version.equalsIgnoreCase("v1_8_R2")) {
            nms = new v1_8_R2();
        } else if (version.equalsIgnoreCase("v1_8_R3")) {
            nms = new v1_8_R3();
        } else if (version.equalsIgnoreCase("v1_9_R1")) {
            nms = new v1_9_R1();
        } else if (version.equalsIgnoreCase("v1_9_R2")) {
            nms = new v1_9_R2();
        } else if (version.equalsIgnoreCase("v1_10_R1")) {
            nms = new v1_10_R1();
        } else if (version.equalsIgnoreCase("v1_11_R1")) {
            nms = new v1_11_R1();
        } else if (version.equalsIgnoreCase("v1_12_R1")) {
            nms = new v1_12_R1();
        } else if (version.equalsIgnoreCase("v1_13_R1")) {
            nms = new v1_13_R1();
        } else {
            getServer().getConsoleSender().sendMessage(Strings.logName + Strings.red + "This plugin wil not work properly with version" + version);
        }
    }
}
