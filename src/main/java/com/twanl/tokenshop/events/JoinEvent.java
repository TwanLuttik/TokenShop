package com.twanl.tokenshop.events;



import com.twanl.tokenshop.TokenShop;
import com.twanl.tokenshop.util.Strings;
import com.twanl.tokenshop.util.UpdateChecker;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinEvent implements Listener {

    private TokenShop plugin = TokenShop.getPlugin(TokenShop.class);

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        final Player p = e.getPlayer();


        // Update message
        if (plugin.getConfig().getBoolean("update_message")) {
            if (p.hasPermission("tokenshop.update")) {


                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public UpdateChecker checker;

                    public void run() {
                        checker = new UpdateChecker(plugin);

                        if (checker.isConnected()) {
                            if (checker.hasUpdate()) {

                                p.sendMessage(Strings.DgrayBS + "----------------------\n");
                                plugin.nms.sendClickableHovarableMessageURL(p, Strings.red + "TokenShop is outdated!", Strings.gold + "Click to go to the download page", "https://www.spigotmc.org/resources/tokens-shop.57866/");
                                p.sendMessage(" \n" +
                                        Strings.white + "Your version: " + plugin.getDescription().getVersion() + "\n" +
                                        Strings.white + "Newest version: " + Strings.green + checker.getLatestVersion() + "\n" +
                                        Strings.DgrayBS + "----------------------");

                            } else {

                                p.sendMessage(Strings.DgrayBS + "----------------------\n" +
                                        Strings.green + "TokenShop is up to date.\n" +
                                        Strings.DgrayBS + "----------------------");

                            }
                        }
                    }
                }, 20);

            }
        }
    }
}
