package com.twanl.tokenshop.menu;

import com.twanl.tokens.api.TokensAPI;
import com.twanl.tokenshop.TokenShop;
import com.twanl.tokenshop.lib.Lib;
import com.twanl.tokenshop.util.ConfigManager;
import com.twanl.tokenshop.util.Strings;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class mainMenu implements Listener {

    private TokenShop plugin = TokenShop.getPlugin(TokenShop.class);
    private ConfigManager cfgM = new ConfigManager();
    private menuApi m = new menuApi();
    private Lib lib = new Lib();
    private TokensAPI A = new TokensAPI();


    @EventHandler
    public void InvenClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        Inventory open = e.getInventory();

        ItemStack item = e.getCurrentItem();

        if (open == null) {
            return;
        }

        for (String key : cfgM.getData().getConfigurationSection("").getKeys(false)) {

            String colorTitle = Strings.translateColorCodes(lib.getTitle(key));

            if (open.getName().equals(colorTitle/*lib.getTitle(key)*/)) {
                e.setCancelled(true);
                if (item == null || !item.hasItemMeta()) {
                    return;
                }

                for (String key1 : cfgM.getData().getConfigurationSection(key + ".slots").getKeys(false)) {

                    int i = Integer.parseInt(key1);


                    String colorText = Strings.translateColorCodes(lib.getItemName(key, i));

                    if (item.getItemMeta().getDisplayName().equals(colorText)) {

                        if (cfgM.getData().isSet(key + ".slots." + i + "permission")) {
                            if (!p.hasPermission(lib.getPermission(key, i))) {
                                p.closeInventory();
                                p.sendMessage(Strings.red + "You don't have permissions to open this");
                                return;
                            }
                        }

                        if (lib.getCommand(key, i).contains("<>")) {
                            p.closeInventory();
                            return;
                        } else if (lib.getCommand(key, i).contains("<*>")) {

                            String[] a = lib.getCommand(key, i).split(" ");
                            String aFinal = a[1];


                            inventory(p, aFinal);
                            return;
                        }

                        int playerBalance = A.playerBalance(p.getUniqueId());
                        int cost = lib.getPrice(key, i);

                        if (cost > playerBalance) {
                            p.closeInventory();
                            p.sendMessage(Strings.red + "You don't have enough tokens!");
                            p.updateInventory();
                        } else {
                            p.closeInventory();
                            String c = Strings.translateColorCodes(lib.getItemName(key, i));
                            p.sendMessage(Strings.gray + "You received " + c);

                            String a = lib.getCommand(key, i);
                            String b = a.replace("{player}", p.getName());


                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), b);
                             A.playerRemoveTokens(p, cost);
                        }

                    }

                }
            }
        }




    }

    public void inventory(Player p, String menu) {

        String colorTitle = Strings.translateColorCodes(lib.getTitle(menu));

        Inventory i = plugin.getServer().createInventory(null, lib.getSlotAmount(menu), colorTitle/*lib.getTitle(menu)*/);
        for (String key : cfgM.getData().getConfigurationSection(menu + ".slots").getKeys(false)) {

            int a = Integer.parseInt(key);
            String colorText = Strings.translateColorCodes(lib.getItemName(menu, a));


            m.addItem(i, colorText, lib.getAmount(menu, a), lib.getSlot(menu, a), lib.getByte(menu, a), Material.getMaterial(lib.getItem(menu, a)), lib.priceStatus1(menu, a), lib.priceStatus(menu, a));

        }
        p.openInventory(i);

    }


    public void saveData() {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();

        ConfigManager cfgM = new ConfigManager();
        cfgM.setup();

        cfgM.saveData();
        cfgM.reloadData();
    }

    public void defaultInv(Player p) {
        String colorTitle = Strings.translateColorCodes(lib.getTitle("menu"));

        Inventory i = plugin.getServer().createInventory(null, lib.getSlotAmount("menu"), colorTitle/*lib.getTitle("menu")*/);
        for (String key : cfgM.getData().getConfigurationSection("menu.slots").getKeys(false)) {

            int a = Integer.parseInt(key);
            String colorText = Strings.translateColorCodes(lib.getItemName("menu", a));

            m.addItem(i, colorText, lib.getAmount("menu", a), lib.getSlot("menu", a), lib.getByte("menu", a), Material.getMaterial(lib.getItem("menu", a)), lib.priceStatus1("menu", a), lib.priceStatus("menu", a));

        }
        p.openInventory(i);

    }





}
