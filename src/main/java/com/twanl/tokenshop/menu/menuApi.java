package com.twanl.tokenshop.menu;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class menuApi {

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, Material itemType) {
        ItemStack I = new ItemStack(itemType, Amount);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        I.setItemMeta(IMeta);

        inv.setItem(itemLocation, I);
    }

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, Material itemType, String lore) {
        ItemStack I = new ItemStack(itemType, Amount);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        ArrayList<String> lore1 = new ArrayList();
        lore1.add(String.valueOf(lore));
        IMeta.setLore(lore1);
        I.setItemMeta(IMeta);

        inv.setItem(itemLocation, I);
    }

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, Material itemType, Enchantment Enchant, int i, boolean b) {
        ItemStack I = new ItemStack(itemType, Amount);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        IMeta.addEnchant(Enchant, i, b);
        I.setItemMeta(IMeta);

        inv.setItem(itemLocation, I);
    }

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, int bty, Material itemType) {
        ItemStack I = new ItemStack(itemType, Amount, (short)bty);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        I.setItemMeta(IMeta);

        inv.setItem(itemLocation, I);
    }

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, int bty, Material itemType, String lore1) {
        ItemStack I = new ItemStack(itemType, Amount, (short)bty);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        ArrayList<String> lore = new ArrayList();
        if (!lore1.isEmpty()) {
            lore.add(String.valueOf(lore1));
        }
        IMeta.setLore(lore);
        I.setItemMeta(IMeta);
        inv.setItem(itemLocation, I);
    }

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, int bty, Material itemType, String lore1, String lore2) {
        ItemStack I = new ItemStack(itemType, Amount, (short)bty);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        ArrayList<String> lore = new ArrayList();
        if (!lore1.isEmpty()) {
            lore.add(String.valueOf(lore1));
        }
        if (!lore2.isEmpty()) {
            lore.add(String.valueOf(lore2));
        }
        IMeta.setLore(lore);
        I.setItemMeta(IMeta);
        inv.setItem(itemLocation, I);
    }

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, int bty, Material itemType, String lore1, String lore2, String lore3) {
        ItemStack I = new ItemStack(itemType, Amount, (short)bty);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        ArrayList<String> lore = new ArrayList();
        if (!lore1.isEmpty()) {
            lore.add(String.valueOf(lore1));
        }
        if (!lore2.isEmpty()) {
            lore.add(String.valueOf(lore2));
        }
        if (!lore3.isEmpty()) {
            lore.add(String.valueOf(lore3));
        }
        IMeta.setLore(lore);
        I.setItemMeta(IMeta);
        inv.setItem(itemLocation, I);
    }

    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, int bty, Material itemType, String lore1, String lore2, String lore3, String lore4) {
        ItemStack I = new ItemStack(itemType, Amount, (short)bty);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        ArrayList<String> lore = new ArrayList();
        if (!lore1.isEmpty()) {
            lore.add(String.valueOf(lore1));
        }
        if (!lore2.isEmpty()) {
            lore.add(String.valueOf(lore2));
        }
        if (!lore3.isEmpty()) {
            lore.add(String.valueOf(lore3));
        }
        if (!lore4.isEmpty()) {
            lore.add(String.valueOf(lore4));
        }
        IMeta.setLore(lore);
        I.setItemMeta(IMeta);
        inv.setItem(itemLocation, I);
    }

    /*
    public void addDEV_ITEM(Inventory inv, String ItemName, int Amount, int itemLocation, Material itemType, int bty, String lore1, String lore2, String lore3, String lore4) {
        ItemStack I = new ItemStack(itemType, Amount, (short)bty);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);

        ArrayList<String>lore = (ArrayList<String>) plugin.getConfig().getStringList("Arenas_GUI." + name + ".Lore");
        for (int i = 0; i < lore.size(); i++) {
            String linha = formatMessage(lore.get(i));
            meta.getLore().add(linha);
        }

        ArrayList<String> lore = new ArrayList();
        if (!lore1.isEmpty()) {
            lore.add(String.valueOf(lore1));
        }
        if (!lore2.isEmpty()) {
            lore.add(String.valueOf(lore2));
        }
        if (!lore3.isEmpty()) {
            lore.add(String.valueOf(lore3));
        }
        if (!lore4.isEmpty()) {
            lore.add(String.valueOf(lore4));
        }
        IMeta.setLore(lore);
        I.setItemMeta(IMeta);
        inv.setItem(itemLocation, I);
    }
    */




    public void addItem(Inventory inv, String ItemName, int Amount, int itemLocation, int bty, Material itemType, Enchantment Enchant, int i, boolean b) {
        ItemStack I = new ItemStack(itemType, Amount, (short)bty);
        ItemMeta IMeta = I.getItemMeta();
        IMeta.setDisplayName(ItemName);
        IMeta.addEnchant(Enchant, i, b);
        I.setItemMeta(IMeta);

        inv.setItem(itemLocation, I);
    }










}
